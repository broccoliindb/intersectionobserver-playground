import React from 'react'
import axios from 'axios'

class MovieScrollComponent extends React.Component {
  constructor() {
    super()
    this.state = {
      movies: [],
      loading: false,
      page: 1,
      prevY: 0
    }
    console.log('b', this.state.page, this.state.prevY)
  }

  getMovies = async (page = 1) => {
    const api = axios.create({
      baseURL: 'https://yts-proxy.nomadcoders1.now.sh'
    })
    try {
      const {
        data: {
          data: { movies }
        }
      } = await api.get('/list_movies.json', {
        params: {
          page,
          limit: 20
        }
      })
      this.setState({
        movies: [...this.state.movies, ...movies]
      })
    } catch (err) {
      console.log(err)
    } finally {
      this.setState({
        loading: false
      })
    }
  }

  handleObserver(entities, observer) {
    const y = entities[0].boundingClientRect.y
    console.log('a', y, this.state.prevY)
    if (this.state.prevY > y) {
      const lastMovie = this.state.movies[this.state.movies.length - 1]
      const curPage = this.state.page
      this.getMovies(curPage + 1)
      this.setState({ page: curPage + 1 })
    }
    this.setState({ prevY: y })
  }
  componentDidMount() {
    this.getMovies(this.state.page)
    var options = {
      root: null,
      rootMargin: '0px',
      threshold: 1.0
    }

    this.observer = new IntersectionObserver(
      this.handleObserver.bind(this),
      options
    )
    this.observer.observe(this.loadingRef)
  }
  render() {
    const loadingCSS = {
      height: '100px',
      margin: '30px'
    }
    const loadingTextCSS = {
      display: this.state.loading ? 'block' : 'none'
    }
    return (
      <div className="container">
        <div style={{ minHeight: '800px' }}>
          {this.state.movies.map((movie, index) => (
            <div key={index}>{movie.title}</div>
          ))}
        </div>
        <div
          ref={(loadingRef) => (this.loadingRef = loadingRef)}
          style={loadingCSS}
        >
          <span style={loadingTextCSS}>Loading...</span>
        </div>
      </div>
    )
  }
}

export default MovieScrollComponent
