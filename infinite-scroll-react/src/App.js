import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Loader from './Loader'
import ScrollComponent from './ScrollComponent'
import MovieScrollComponent from './MovieScrollComponent'
import MovieScrollHook from './MovieScrollHook'
// const api = axios.create({
//   baseURL: "https://yts-proxy.nomadcoders1.now.sh",
// });

// const throttle = (fn, ms) => {
//   let timeout;
//   const exec = () => fn.apply();
//   const clear = () => (timeout === undefined ? null : clearTimeout(timeout));
//   if (fn !== undefined && ms !== undefined) {
//     timeout = setTimeout(exec, ms);
//   }
//   throttle.clearTimeout = () => clear();
// };

// function App() {
//   const [results, setResults] = useState([]);
//   const [error, setError] = useState(null);
//   const [loading, setLoading] = useState(true);
//   const [page, setPage] = useState(1);

//   const isEnded = () => {
//     const pageHeight = document.documentElement.getBoundingClientRect().height
//     const top =
//   }

//   const fetchData = async (page = 1) => {
//     try {
//       const {
//         data: {
//           data: { movies },
//         },
//       } = await api.get("/list_movies.json", {
//         params: {
//           page,
//           limit: 50,
//         },
//       });
//       setResults(movies);
//     } catch (err) {
//       console.log(err);
//       setError("error ...");
//     } finally {
//       setLoading(false);
//     }
//   };

//   useEffect(() => {
//     fetchData(page);
//   }, []);
//   return loading ? (
//     <div className="App">
//       <h1>Movie List</h1>
//       <Loader />
//     </div>
//   ) : error ? (
//     error.toString()
//   ) : (
//     <div className="App">
//       <h1>Movie List</h1>
//       {newPage}
//       <ul>
//         {results.map((movie) => (
//           <li key={movie.id}>{movie.title}</li>
//         ))}
//       </ul>
//     </div>
//   );
// }

// export default App;

function App() {
  return (
    <div className="App">
      <h1>Movie List</h1>
      <MovieScrollHook />
    </div>
  )
}

export default App
