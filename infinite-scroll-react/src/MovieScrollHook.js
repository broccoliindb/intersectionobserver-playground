import React, { useState, useEffect, useRef } from 'react'
import axios from 'axios'
import { render } from '@testing-library/react'

const MovieScrollHook = () => {
  const [movies, setMovies] = useState([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(0)
  const loadingRef = useRef()

  const loadingCSS = {
    height: '100px'
  }
  const loadingTextCSS = {
    display: loading ? 'block' : 'none'
  }
  const getMovies = async (page) => {
    if (page > 0) {
      const api = axios.create({
        baseURL: 'https://yts-proxy.nomadcoders1.now.sh'
      })
      setLoading(true)
      try {
        const {
          data: {
            data: { movies: newMovies }
          }
        } = await api.get('/list_movies.json', {
          params: {
            page,
            limit: 50
          }
        })
        if (movies.length === 0) {
          setMovies([...newMovies])
        } else {
          setMovies([...movies, ...newMovies])
        }
      } catch (err) {
        console.log(err)
      } finally {
        setLoading(false)
      }
    }
  }

  const handleObserver = (entities, observer) => {
    const { isIntersecting, intersectionRatio } = entities[0]
    if (isIntersecting) {
      setPage((p) => p + 1)
    }
  }

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: '0px',
      threshold: 0
    }
    const observer = new IntersectionObserver(handleObserver, options)
    observer.observe(loadingRef.current)
  }, [])

  useEffect(() => {
    getMovies(page)
  }, [page])

  return (
    <>
      <div>
        {movies.map((movie, index) => {
          return <div key={index}>{movie.title}</div>
        })}
      </div>
      <div ref={loadingRef} style={loadingCSS}>
        <span style={loadingTextCSS}>Loading...</span>
      </div>
    </>
  )
}

export default MovieScrollHook
