const container = document.querySelector('.container')
const loader = document.querySelector('.loader')

const getFetch = (detailedUrl, params) => {
  const baseUrl = 'https://yts-proxy.nomadcoders1.now.sh'
  const getQueryString = (params) => {
    const esc = encodeURIComponent
    return Object.keys(params)
      .map((k) => esc(k) + '=' + esc(params[k]))
      .join('&')
  }
  const options = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return fetch(`${baseUrl}${detailedUrl}?${getQueryString(params)}`)
}

const getMovieList = async (page) => {
  const movieData = []
  const params = {
    page: page,
    limit: 50
  }
  let error = null
  let loading = true
  if (!loading) {
    loader.classList.remove('loading')
  } else {
    loader.classList.add('loading')
  }
  try {
    const response = await getFetch('/list_movies.json', params)
    if (response.ok) {
      const {
        data: { movies }
      } = await response.json()
      movieData.push(...movies)
    }
  } catch (err) {
    error = err
    console.log(err)
  } finally {
    loading = false
  }
  return {
    loading,
    error,
    movieData
  }
}

const compositeMovieList = async (page) => {
  const { loading, error, movieData } = await getMovieList(page)
  setMovieList(loading, error, movieData)
}

const setInfiniteScroll = (page) => {
  const handleObserver = (entries, observer) => {
    const { isIntersecting } = entries[0]
    if (isIntersecting) {
      compositeMovieList(++page)
    }
  }
  const opts = {
    root: null,
    rootMargin: '0px',
    threshold: 1.0
  }
  const observer = new IntersectionObserver(handleObserver, opts)
  observer.observe(loader)
}

const setMovieList = (loading, error, movieData) => {
  const fragment = new DocumentFragment()

  if (movieData && movieData.length) {
    movieData.forEach((i) => {
      const li = document.createElement('li')
      li.innerHTML = i.title
      fragment.appendChild(li)
    })
    container.appendChild(fragment)
    if (!loading) {
      loader.classList.remove('loading')
    } else {
      loader.classList.add('loading')
    }
  }
}

const init = async () => {
  let page = 1
  await compositeMovieList(page)
  setInfiniteScroll(page)
}

window.addEventListener('load', init)
