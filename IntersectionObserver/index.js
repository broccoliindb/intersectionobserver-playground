let box
let color1 = 'rgba(23,24,123,ratio)'
let color2 = 'rgba(45,223,213,ratio)'
// let color1 = 'rgba(23,24,123)'
// let color2 = 'rgba(45,223,213)'
let numSteps = 20
let prevRatio = 0.0

const handleIntersect = (entries, observer) => {
  console.log(entries)
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio) {
      entry.target.style.backgroundColor = color1.replace(
        'ratio',
        entry.intersectionRatio
      )
      // entry.target.style.backgroundColor = 'grey'
    } else {
      // entry.target.style.backgroundColor = 'pink'

      entry.target.style.backgroundColor = color2.replace(
        'ratio',
        entry.intersectionRatio
      )
    }

    prevRatio = entry.intersectionRatio
  })
}

const buildThresholdList = () => {
  let thresholds = []
  let numSteps = 20

  for (let i = 1.0; i <= numSteps; i++) {
    let ratio = i / numSteps
    thresholds.push(ratio)
  }
  thresholds.push(0)
  return thresholds
}

const createObserver = () => {
  let options = {
    root: document.querySelector('.container'),
    // root: null,
    rootMargin: '-10px',
    threshold: [1, 0.5]
    // threshold: buildThresholdList()
    //ddd
  }

  const observer = new IntersectionObserver(handleIntersect, options)
  observer.observe(box)
}

window.addEventListener('load', (evt) => {
  box = document.querySelector('.box')
  createObserver()
})
